﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Text;
using QuizDataAccess.Models;

namespace QuizDataAccess.DataAccess
{
    public class QuizDataAccess : DbContext
    {
        public QuizDataAccess(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Participant> Participants { get; set; }
        public DbSet<QuizParticipant> QuizParticipants { get; set; }
        public DbSet<Quiz> Quizzes { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<Image> Images { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<User>(entity =>
            //{
            //    entity.HasKey(e => e.Id);
            //    entity.HasOne(d => d.UserPicture)
            //        .WithOne(userPict => userPict.User)
            //        .OnDelete(DeleteBehavior.Cascade);

            //});
            modelBuilder.Entity<QuizParticipant>(entity =>
            {
                entity.HasOne(qp => qp.Participant)
                .WithMany(p => p.QuizParticipations)
                .HasForeignKey(qp => qp.ParticipantId)
                .OnDelete(DeleteBehavior.Cascade);

                entity.HasOne(qp => qp.Quiz)
                .WithMany(qp => qp.QuizParticipants)
                .HasForeignKey(qp => qp.QuizId)
                .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Question>(entity =>
            {
                entity.HasOne(q => q.Quiz)
                .WithMany(quiz => quiz.Questions)
                .HasForeignKey(q => q.QuizId)
                .OnDelete(DeleteBehavior.Cascade);

                
            });
            modelBuilder.Entity<Image>(entity =>
            {
                entity.HasOne(i => i.Question).WithOne(q => q.Image);       

            });

        }
    }
}
