﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuizDataAccess.Models
{
    public class Image : BaseEntity
    {
        public int Id { get; set; }
        public string DiskUrl { get; set; }
        public int QuestionId { get; set; }
        public Question Question { get; set; }
    }
}
