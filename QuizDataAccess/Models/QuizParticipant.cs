﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuizDataAccess.Models
{
    public class QuizParticipant : BaseEntity
    {
        public int Id { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public int CorrectAnswers { get; set; }
        public int ParticipantId { get; set; }
        public int QuizId { get; set; }
        public Participant Participant { get; set; }
        public Quiz Quiz { get; set; }


    }
}
