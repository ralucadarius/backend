﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuizDataAccess.Models
{
    public class Quiz : BaseEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<QuizParticipant> QuizParticipants { get; set; }
        public ICollection<Question> Questions { get; set; }
       
    }
}
