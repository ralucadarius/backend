﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuizDataAccess.Models
{
    public class Question : BaseEntity
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int TimeToAnswer { get; set; }
        public int Order { get; set; }
        public int QuizId { get; set; }
        public Quiz Quiz { get; set; }
        public Image Image { get; set; }
        public ICollection<Answer> Answers { get; set; }
    }
}
