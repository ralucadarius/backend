﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuizDataAccess.Models
{
    public class Participant : BaseEntity
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

        public ICollection<QuizParticipant> QuizParticipations { get; set; }
    }
}
